const azure = require('azure-storage');
const util = require('util');

const accountname = "dxpimages";
const key = "ep2914PByZNhcD8GR8b8KvPeqnXKdGG5eqpoPgBpB4NFzMCT5huUAlBLPr0Wo+tAh+H/9UtZwNpUjRev1ZWb7w==";

module.exports = async function (context, req) {

    if(req.body.mode=="saveToDB"){
        let tableName=req.body.tableName;
        let entity=req.body.entity;
        const result=await saveToDB(tableName,entity);
        context.res = {
            status: 200, /* Defaults to 200 */
            error: false,
            body: result
        };
        context.done();
        return;
    }

    if(req.body.mode = 'generateTextEntity') {
        
        textEntity = req.body.textEntity;
        var entity = generateTextEntity(textEntity);
    }
    context.res = {
        status: 200, /* Defaults to 200 */
        body: entity
    };
}

async function generateTextEntity(textEntity){

    var entGen = azure.TableUtilities.entityGenerator;
             
        textEntity = {
            PartitionKey: entGen.String(textEntity.partitionKey[0]),
            RowKey: entGen.String(textEntity.rowKey[0]),
            status: entGen.String(textEntity.status[0]),
            id_inference_raw: entGen.String(textEntity.id_inference_raw[0]),
            id_inference_processed: entGen.String(textEntity.id_inference_processed[0]),
            dateCreated: entGen.DateTime(textEntity.dateCreated[0])
        }
    return textEntity;
}

async function saveToDB(tableName,entity){
    const tableClient = azure.createTableService(accountname, key);
    let insertEntityInTableAsync = util.promisify(tableClient.insertOrMergeEntity);
    insertEntityInTableAsync = insertEntityInTableAsync.bind(tableClient);
    let result = { error: false };
    try {
        result.result = await insertEntityInTableAsync(tableName, entity);
    } catch (err) {
        result.error = true;
        result.result = err;
        return result;
    }
    if (result) return result;
}

async function postDBData(tableName, entity) {
    console.log("Inside postDBData");
    const tablePostOptions = {
        uri: DBServiceUri,
        body: {
            'tableName': tableName,
            'entity': entity,
            'mode': "saveToDB"
        },
        json: true,
        headers: {
            'Content-Type': 'application/json',
        }
    };
    let result = await callAPI(tablePostOptions);
    return result;
}

async function callAPI(requestOptions){

    let postAsync = util.promisify(request.post);
    let result = { error: false };
    try {
        result.result = await postAsync(requestOptions);
    } catch (err) {
        result.error = true;
        result.result = err;
        console.log('error', err);
        return result;
    }
    return result;
}
const streamifier = require('streamifier');
const { parse } = require('azure-func-multipart-parser');
const azure = require('azure-storage');
const request = require('request');
const util = require('util');
const sharp = require('sharp');
const { v4: uuidv4 } = require('uuid');

const blobServiceUri = "http://localhost:7071/api/BlobService";
//70e3feeea4c644e8929d4ebae5ee8d18
const faceSubscriptionKey = "e027265950cb476da48bc64446cca57b";
const faceEndpoint = "https://dxpface.cognitiveservices.azure.com/";
const faceParams = "?returnFaceAttributes=age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise";
const faceUriBase = faceEndpoint + 'face/v1.0/detect' + faceParams;
const DBServiceUri = 'http://localhost:7071/api/AzureDBService';
const resourceNames = {
    rawImageContainer: "rawimages",
    faceImageContainer: "faceimages",
    idtype: ['id', 'address', "age"],
    statuses: ["no_activity", "id_uploaded", "address_uploaded", "age_uploaded", "form_submitted", "otp_generated", "otp_verified", "kyc_verified"],
    PartitionKeyUser: 'userdetails',
    PartitionKeyOtp: 'otp'
}


module.exports = async function (context, req) {

    console.log('FACE API hit');

    if (req.body.mode == 'getFaceData') {
        let filedata = req.body.filedata;
        let result = await getFaceData(filedata);
        context.res = {
            status: 200, /* Defaults to 200 */
            error: false,
            body: result
        };
    }

    if (req.body.mode == 'processFaceData') {

        let resp = req.body.resp;
        let filename = req.body.filename;
        let filedata = req.body.filedata
        let uuid = req.body.uuid;
        let result = await processFaceData(resp, filename, filedata, uuid);
        context.res = {
            status: 200, // Defaults to 200 
            error: false,
            body: result
        };
        /*if(result==false)
        {
            context.res = {
                status: 500, // Defaults to 200 
                error: true,
                body: resp,
                message:"No data found by Face API"
            };
            context.done();
        }
        else
        {
            context.res = {
                status: 200, // Defaults to 200 
                error: false,
                body: result
            };
        }*/
        
    }

}
function isEmpty(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
}
async function processFaceData(faceResp, filename, filedata, uuid) {
    //console.log("Inside processFaceData in FACE API", faceResp.result.body.lenght);
    // condition need to be change
    if(isEmpty(JSON.parse(faceResp.result.body)))
    {
        
        return;

    }
    else
    {
        resp = JSON.parse(faceResp.result.body)[0].faceRectangle;
    
    
        let { height, width } = await sharp(new Buffer(filedata)).metadata();
        let tempResp = {}
        tempResp.top = Math.floor(resp.top - resp.height * 0.35);
        tempResp.left = Math.floor(resp.left - resp.width * 0.15);
        tempResp.height = Math.floor(resp.height * 1.8);
        tempResp.width = Math.floor(resp.width * 1.4);

        tempResp.top = tempResp.top < 0 ? 0 : tempResp.top;
        tempResp.left = tempResp.left < 0 ? 0 : tempResp.left;

        tempResp.height = tempResp.top + tempResp.height > height ? (height - tempResp.top) : tempResp.height;
        tempResp.width = tempResp.left + tempResp.width > width ? (width - tempResp.left) : tempResp.width;

        let ff = await sharp(new Buffer(filedata))
            .extract({ width: tempResp.width, height: tempResp.height, left: tempResp.left, top: tempResp.top })
            .resize(200, 250)
            .toBuffer();
        let faceResult = await postBlobData(resourceNames.faceImageContainer, filename, ff, { contentSettings: { contentType: 'image/jpeg' } });

        // console.log('faceresult', faceResult);
        var entGen = azure.TableUtilities.entityGenerator;
        let faceEntity = {
            PartitionKey: entGen.String('facedesc'),
            RowKey: entGen.String(uuid),
            //RowKey: entGen.String('10137'),
            //inferenceData: entGen.String(JSON.stringify(faceResponse[0]))
            inferenceData: entGen.String(JSON.stringify(faceResp.result.body)) 
         }

        let entityResult = await postDBData('facedata', faceEntity); //postDBData function
        // context.log(entityResult);
        return entityResult;
        
    }
}

async function postBlobData(containerName, filename, filedata, options) {

    console.log("Inside postBlobData in FACE API");
    const blobPostOptions = {
        uri: blobServiceUri,
        body: {
            'containerName': containerName,
            'filename': filename,
            'filedata': filedata,
            'options': options
        },
        json: true,
        headers: {
            'Content-Type': 'application/json',
        }
    };

    let postAsync = util.promisify(request.post);
    let result = { error: false };
    try {
        result.result = await postAsync(blobPostOptions);
    } catch (err) {
        result.error = true;
        result.result = err;
        console.log('error', err);
        return result;
    }

    if (result.result) return result.result.body;
}

async function getFaceData(filedata) {
    console.log("Inside getFaceData");
    const facePostOptions = {
        uri: faceUriBase,
        body: new Buffer(filedata),
        headers: {
            'Content-Type': 'application/octet-stream',
            'Ocp-Apim-Subscription-Key': faceSubscriptionKey
        }
    };
    // let postAsync = util.promisify(request.post);
    // let result = { error: false };
    // try {
    //     result.result = await postAsync(facePostOptions);
        
    // } catch (err) {
    //     result.error = true;
    //     result.result = err;
    //     return result;
    // }
    let result =  await callAPI(facePostOptions);
    return result;
}

async function postDBData(tableName, entity) {
    console.log("Inside postDBData");
    const tablePostOptions = {
        uri: DBServiceUri,
        body: {
            'tableName': tableName,
            'entity': entity,
            'mode': "saveToDB"
        },
        json: true,
        headers: {
            'Content-Type': 'application/json',
        }
    };
    let result = callAPI(tablePostOptions);
    return result;
}

async function callAPI(requestOptions){

    let postAsync = util.promisify(request.post);
    let result = { error: false };
    try {
        result.result = await postAsync(requestOptions);
    } catch (err) {
        result.error = true;
        result.result = err;
        console.log('error', err);
        return result;
    }
    return result;
}
const streamifier = require('streamifier');
const { parse } = require('azure-func-multipart-parser');
const azure = require('azure-storage');
const request = require('request');
const util = require('util');
const sharp = require('sharp');
const { v4: uuidv4 } = require('uuid');

//b0451e2f2877459484182073fc85948b
//70e3feeea4c644e8929d4ebae5ee8d18//
const ocrSubscriptionKey = "70e3feeea4c644e8929d4ebae5ee8d18";
const ocrEndpoint = "https://dxp.cognitiveservices.azure.com/"
const ocrUriBase = ocrEndpoint + 'vision/v3.1/ocr';

let params = {
    'language': 'en',
    'detectOrientation': 'true',
};

const DBServiceUri = 'http://localhost:7071/api/AzureDBService';

const resourceNames = {
    rawImageContainer: "rawimages",
    faceImageContainer: "faceimages",
    idtype: ['id', 'address', "age"],
    statuses: ["no_activity", "id_uploaded", "address_uploaded", "age_uploaded", "form_submitted", "otp_generated", "otp_verified", "kyc_verified"],
    PartitionKeyUser: 'userdetails',
    PartitionKeyOtp: 'otp'
}

module.exports = async function (context, req) {

    console.log('OCR Service Request', Object.keys(req.body));
    if (req.body.mode == 'processOCRData') {

        let jsonResponse = req.body.jsonResponse;
        let uuid = req.body.uuid;
        let idtype = req.body.idtype;
        let processedData = await processOCRData(jsonResponse, uuid, idtype);
        context.res = {
            body: {
                error: false,
                inference: processedData,
                uuid: uuid
            }
        }
    }
    else {

        let filedata = req.body.filedata;
        let result = await getOCRData(filedata);
        // console.log('file ocr data', result);
        context.res = {
            body: {
                error: false,
                result : result
            }
        };
    }
}

async function processOCRData(jsonResponse, uuid, idtype) {

    //console.log('*****',jsonResponse.result.body, typeof jsonResponse.result.body);
    console.log("##inside processOCRData Service##");
    let processedData = [];
    //console.log("json to process ", jsonResponse);
    let jsonResp = JSON.parse(jsonResponse.result.body.result);
    console.log("$$$jsonResponse for process$$$", jsonResp);
    if(jsonResp.regions.length!=0)
    {
        jsonResp.regions.forEach((d, i) => {
            d.lines.forEach((dd, ii) => {
                processedData.push((dd.words.map(ddd => ddd.text)).join(" "));
                //processedData.join(" ");
            })
        
        })
        data = {"Card_Issued_By": "",
                "Card_Holder_Country": "",
                "Id":"",
			    "Name":""
                }
        var ar, val;
        const regexR = new RegExp('[^a-zA-Z]*');
        const regexNm = new RegExp('Name[a-z]*');
        const regexNationality = new RegExp('Nationality[a-z]*');
        const regexU = new RegExp('United Arab Emirates*');
        // regex to validate the format xxx-xxxx-xxxxxxx-x (Emirates ID)
        const regexId = new RegExp('^784-[0-9]{4}-[0-9]{7}-[0-9]{1}$');
        for(ar of processedData)
        {
           if(regexU.test(ar))
           {
               data["Card_Issued_By"]="United Arab Emirates";
           }
           if(regexNm.test(ar))
           {
                var d1 = ar.replace(regexNm, '').trim();
                data["Name"]=d1.replace(regexR, '').trim();
           }
           if(regexNationality.test(ar))
           {
                var d2 = ar.replace(regexNationality, '').trim();
                console.log(d2);
                data["Card_Holder_Country"]= d2.replace(regexR, "").trim();
           }
           if(regexId.test(ar))
           {
               data["Id"]=ar;
           }

        }
        console.log("data =>", data);

    }
    else if (processedData.length == 0) {
        context.res = {
            body: {
                error: true,
                message: "No OCR Data found in the Uploaded Image"
            }
        }
        context.done();
        return;
    }

    /*let textEntity;
    if (idtype == resourceNames.idtype[0]) {

        textEntity = {
            partitionKey: [resourceNames.PartitionKeyUser, "String"],
            rowKey: [uuid, "String"],
            status: [resourceNames.statuses[1], "String"],
            id_inference_raw: [jsonResponse.result.body, "String"],
            id_inference_processed: [JSON.stringify(processedData), "String"],
            dateCreated: [new Date(), "DateTime"]
        }
        var tableEntity = await generateTextEntity(textEntity);

    } else if (idtype == resourceNames.idtype[1]) {
        textEntity = {
            partitionKey: [resourceNames.PartitionKeyUser, "String"],
            rowKey: [uuid, "String"],
            status: [resourceNames.statuses[2], "String"],
            id_inference_raw: [jsonResponse.result.body, "String"],
            id_inference_processed: [JSON.stringify(processedData), "String"],
            dateCreated: [new Date(), "DateTime"]
        }
        var tableEntity = await generateTextEntity(textEntity);

    } else if (idtype == resourceNames.idtype[2]) {
        textEntity = {
            partitionKey: [resourceNames.PartitionKeyUser, "String"],
            rowKey: [uuid, "String"],
            status: [resourceNames.statuses[3], "String"],
            id_inference_raw: [jsonResponse.result.body, "String"],
            id_inference_processed: [JSON.stringify(processedData), "String"],
            dateCreated: [new Date(), "DateTime"]
        }
        var tableEntity = await generateTextEntity(textEntity);
    }

    let entityResult = await postDBData('userdata', tableEntity);
    return entityResult;*/
    //return processedData
    console.log("final data =>", processedData);
    return data;
}
async function getOCRData(filedata) {
    console.log("Inside getOCRData", new Buffer(JSON.parse(filedata)));
    const ocrPostOptions = {
        uri: ocrUriBase,
        qs: params,
        body: new Buffer(new Buffer(JSON.parse(filedata))),
        //json: true,
        headers: {
            'Content-Type': 'application/octet-stream',
            'Ocp-Apim-Subscription-Key': ocrSubscriptionKey
            
        }
    };
    let result
    try {
        result = await callAPI(ocrPostOptions);
    } catch (err) {
        console.log('ocr service error', err);
    }

    if (result) {
        console.log('ocr service', Object.keys(result), Object.keys(result.result), result.result.body);
        return result.result.body;
    }
}

async function generateTextEntity(textEntity) {

    console.log("Inside generate Text Entity");
    const textEntityOptions = {
        uri: DBServiceUri,
        body: {
            'textEntity': textEntity
        },
        headers: {
            'Content-Type': 'application/json',
        }
    };
    let result = await callAPI(textEntityOptions);
    return result;

}
async function postDBData(tableName, entity) {
    console.log("Inside postDBData");
    const tablePostOptions = {
        uri: DBServiceUri,
        body: {
            'tableName': tableName,
            'entity': entity,
            'mode': "saveToDB"
        },
        json: true,
        headers: {
            'Content-Type': 'application/json',
        }
    };
    let result = callAPI(tablePostOptions);
    return result;
}
async function callAPI(requestOptions) {

    let postAsync = util.promisify(request.post);
    let result = { error: false };
    try {
        result.result = await postAsync(requestOptions);
    } catch (err) {
        result.error = true;
        result.result = err;
        console.log('error', 'call api ocr service');
        return result;
    }
    return result;
}
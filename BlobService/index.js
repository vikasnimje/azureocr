
const azure = require('azure-storage');
const request = require('request');
const util = require('util');
const streamifier = require('streamifier');


const accountname = "dxpimages";
const key = "ep2914PByZNhcD8GR8b8KvPeqnXKdGG5eqpoPgBpB4NFzMCT5huUAlBLPr0Wo+tAh+H/9UtZwNpUjRev1ZWb7w==";

module.exports = async function (context, req) {


    console.log('inside blob service');
    let containerName = req.body.containerName;
    let filename = req.body.filename;
    let filedata = req.body.filedata;
    let options = req.body.options;

   let result = await saveToBlob(containerName, filename, filedata, options);
     
    context.res = {
        // status: 200, /* Defaults to 200 */
        error: false,
        body: result
    };
}

async function saveToBlob(containerName, filename, filedata, options) {
    filedata = new Buffer(filedata);
    const blobClient = azure.createBlobService(accountname, key);
    let createBlockBlobFromStreamAsync = util.promisify(blobClient.createBlockBlobFromStream);
    createBlockBlobFromStreamAsync = createBlockBlobFromStreamAsync.bind(blobClient);
    let result = { error: false };
    try {
        result = await createBlockBlobFromStreamAsync(containerName, filename, streamifier.createReadStream(new Buffer(filedata)), filedata.length, options);
    } catch (err) {
        result.error = true;
        result.result = err;
        return result;
    }
    if (result) { return result};
}

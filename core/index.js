const streamifier = require('streamifier');
const { parse } = require('azure-func-multipart-parser');
const azure = require('azure-storage');
const request = require('request');
const util = require('util');
const sharp = require('sharp');

const { v4: uuidv4 } = require('uuid');
const fs = require('fs');

const blobServiceUri = "http://localhost:7071/api/BlobService";
const faceApiServiceUri = 'http://localhost:7071/api/AzureFaceAPIService';
const DBServiceUri ='http://localhost:7071/api/AzureDBService';
const ocrServiceUri = 'http://localhost:7071/api/AzureOCRService';

const resourceNames = {
    rawImageContainer: "rawimages",
    faceImageContainer: "faceimages",
    idtype: ['id', 'address', "age"],
    supportedFormat:['JPEG', 'PNG', 'GIF', 'BMP'],
    statuses: ["no_activity", "id_uploaded", "address_uploaded", "age_uploaded", "form_submitted", "otp_generated", "otp_verified", "kyc_verified"],
    PartitionKeyUser: 'userdetails',
    PartitionKeyOtp: 'otp'
}

module.exports = async function (context, req) {
    context.log('JavaScript HTTP trigger function processed a request.');


    const { fields, files } = parse(req);
    console.log(fields, files.idcard);
    if (fields.idtype === undefined || files.idcard === undefined) {
        context.log("idtype parameter or file attachment is missing in post request");
        context.res = {
            body: {
                error: true,
                message: "idtype parameter or file attachment is missing in post request"
            }
        }
        context.done();
        return;
    }

    if (!resourceNames.idtype.includes(fields.idtype)) {
        context.log("requested idtype is not supported");
        context.res = {
            body: {
                error: true,
                message: "requested idtype is not supported"
            }
        }
        context.done();
        return;
    }

    var filedata = files.idcard.content;
    console.log("filedata", filedata);
    var filename = files.idcard.filename;
    console.log("before sharp filename", files.idcard.filename );
    filename = filename.split('.');
    filename = filename[filename.length - 1];

    console.log("File type ->", files.idcard.type.split('/')[1].toUpperCase());
    
    const scaledImg = await sharp(filedata)
    //.toColourspace('rgb16')
    .modulate({
        brightness: 1})
        .resize({ width: 950, height: 650 })
    .toFormat("jpeg")
   
    .jpeg({ quality: 100,
            chromaSubsampling: '4:4:4' })
    .toBuffer();
    
    if (!resourceNames.supportedFormat.includes(files.idcard.type.split('/')[1].toUpperCase())) {
        context.log("requested file format is not supported!");
        context.res = {
            body: {
                error: true,
                message: "Requested file format is not supported!! Please use JPEG, PNG, GIF, BMP."
            }
        }
        context.done();
        return;
    }
   
    
    //filedata = scaledImg;    
         
    // let uuid = uuidv4();
    let uuid = fields.uuid;
    filename = uuid + '_' + fields.idtype + '.' + filename;
    console.log("Filename=>", filename);
    var options = {
        contentSettings: { contentType: files.idcard.type }
    };
    let result;
    try {
        result = await postBlobData(resourceNames.rawImageContainer, filename, filedata, options);
        const etag = result.etag;
        console.log('blob result ',result);
    }
    catch (err) {
        console.log('error', 'post blob data');
    }

    ///////////////////////////////////////////////////////
    let faceResponse, jsonResponse;

    if (!result.error) {
        context.log("Image uploaded successfully");
        jsonResponse = await getOCRData(scaledImg)
        console.log("got jsonResponse");
        /*context.res = {
            body: {
                "error": false,
                data : jsonResponse
            }
        } */  
       // console.log('json response I',jsonResponse);
        if (fields.idtype == resourceNames.idtype[0]) 
           faceResponse = await getFaceData(filedata);
           context.res = {
            body: {
                "error": false,
                data : faceResponse
            }
        }
        //context.done();
    } else {
       
        context.res = {
            body: {
                "error": true,
                message: "Error while uploading blob"
            }
        }
        context.done();
    }

    // analysizing faceResponse
    if (fields.idtype == resourceNames.idtype[0]) {
        if (!faceResponse.error) {
            // context.log('resp', faceResponse.result.body);
            faceResponse = faceResponse.result.body;
            let resp = faceResponse;

            if (resp == undefined) {
                context.res = {
                    body: {
                        error: true,
                        message: "No Face picture found in ID Proof"
                    }
                }
                context.done();
                return;
            }

           if (resp) {
            let faceResult = await processFaceData(resp,filename,filedata, uuid);
           }
        } else {
            // context.log(faceResponse.result);
            context.res = {
                body: {
                    error: true,
                    message: "Error While making Face API Request"
                }
            }
            context.done();
        }    
    } 

    //analyzing jsonResponse
    if (!jsonResponse.error) {
        idtype = fields.idtype;
        let ocrResult = await processOCRData(jsonResponse, uuid, idtype);
        context.res = {
            body: {
                
                error: false,
                data : ocrResult

            }
        }
        context.done();

    } else {
        // context.log(jsonResponse.result)
        context.res = {
            body: {
                error: true,
                message: "Error While making OCR Request"
            }
        }
        context.done();
    }
}


async function postBlobData(containerName, filename, filedata, options) {

    const blobPostOptions = {
        uri: blobServiceUri,
        body: {
            'containerName': containerName,
            'filename': filename,
            'filedata': filedata,
            'options': options
        },
        json: true,
        headers: {
            'Content-Type': 'application/json',
        }
    };

   let result = await callAPI(blobPostOptions);
   if (result.result) return result.result.body;
}

async function getFaceData(filedata) {
    console.log("Inside getFaceData core");
    const facePostOptions = {
        uri: faceApiServiceUri,
        body: {
            'filedata': new Buffer(filedata),
            'mode':"getFaceData"
        },
        json: true,
        headers: {
            'Content-Type': 'application/json',
        }
    };
    let result = await callAPI(facePostOptions);
    return result;
}

async function getOCRData(filedata) {
    console.log("Inside getOCRData core", filedata);
    const ocrPostOptions = {
        uri: ocrServiceUri,
        json: true,
        headers: {
            'Content-Type': 'application/json',
        },
        body : {
            'filedata' : JSON.stringify(filedata),
            'mode':'getOCRData'
        }
    };
    let result = await callAPI(ocrPostOptions);
   // console.log("core **************", result);
    return result;
}

async function processOCRData(jsonResponse,uuid,idtype){
    console.log("##Inside processOCRData core##");
    const ocrPostOptions = {
        uri: ocrServiceUri,
        body: {
            'jsonResponse': jsonResponse,
            'uuid' :uuid,
            'idtype':idtype,
            'mode' : "processOCRData"
        },
        json: true,
        headers: {
            'Content-Type': 'application/json',
        }
    };
    let result = await callAPI(ocrPostOptions);
    return result;
}

async function processFaceData(resp,filename,filedata, uuid){
    console.log("##Inside processFaceData core##");
    const facePostOptions = {
        uri: faceApiServiceUri,
        body: {
            'resp': resp,
            'filename': filename,
            'filedata': filedata,
            'uuid': uuid,
            'mode' : "processFaceData"
        },
        json: true,
        headers: {
            'Content-Type': 'application/json',
        }
    };
    let result = await callAPI(facePostOptions);
    return result;
}

async function postDBData(tableName, entity) {
    console.log("Inside core postDBData");
    const tablePostOptions = {
        uri: DBServiceUri,
        body: {
            'tableName': tableName,
            'entity': entity,
            'mode': "saveToDB"
        },
        json: true,
        headers: {
            'Content-Type': 'application/json',
        }
    };
    let result = await callAPI(tablePostOptions);
    return result;
}

async function callAPI(requestOptions){

    let postAsync = util.promisify(request.post);
    let result = { error: false };
    try {
        result.result = await postAsync(requestOptions);
    } catch (err) {
        result.error = true;
        result.result = err;
        console.log('error', 'core call api');
        return result;
    }
    return result;
}